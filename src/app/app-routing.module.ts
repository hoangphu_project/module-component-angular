import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'module1',
    loadChildren: () =>
      import('./module1/module1.module').then((res) => res.Module1Module),
  },
  {
    path: 'module2',
    loadChildren: () =>
      import('./module2/module2.module').then((res) => res.Module2Module),
  },
  { path: '', redirectTo: '/module1/hero-a', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
