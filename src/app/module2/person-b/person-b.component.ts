import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-person-b',
  templateUrl: './person-b.component.html',
  styleUrl: './person-b.component.scss',
})
export class PersonBComponent {
  @Input() warnNotify: string = '';
}
