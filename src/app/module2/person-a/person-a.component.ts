import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-person-a',
  templateUrl: './person-a.component.html',
  styleUrl: './person-a.component.scss',
})
export class PersonAComponent {
  @Output() newWarnEvent = new EventEmitter<string>();

  Warn() {
    this.newWarnEvent.emit('Help me from Person A');
  }
}
