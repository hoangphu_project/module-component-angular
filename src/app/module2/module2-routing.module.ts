import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonAComponent } from './person-a/person-a.component';
import { PersonBComponent } from './person-b/person-b.component';

const routes: Routes = [
  { path: 'person-a', component: PersonAComponent },
  { path: 'person-b', component: PersonBComponent },
  { path: '', redirectTo: 'person-a', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Module2RoutingModule {}
