import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonAComponent } from './person-a/person-a.component';
import { PersonBComponent } from './person-b/person-b.component';
import { Module2RoutingModule } from './module2-routing.module';

@NgModule({
  declarations: [PersonAComponent, PersonBComponent],
  imports: [CommonModule, Module2RoutingModule],
})
export class Module2Module {}
