import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hero-b',
  templateUrl: './hero-b.component.html',
  styleUrl: './hero-b.component.scss',
})
export class HeroBComponent {
  @Input() hello: string = '';
}
