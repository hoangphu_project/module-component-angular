import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroAComponent } from './hero-a/hero-a.component';
import { HeroBComponent } from './hero-b/hero-b.component';

const routes: Routes = [
  { path: 'hero-a', component: HeroAComponent },
  { path: 'hero-b', component: HeroBComponent },
  { path: '', redirectTo: 'hero-a', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Module1RoutingModule {}
