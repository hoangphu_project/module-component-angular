import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroAComponent } from './hero-a/hero-a.component';
import { HeroBComponent } from './hero-b/hero-b.component';
import { Module1RoutingModule } from './module1-routing.module';

@NgModule({
  declarations: [HeroAComponent, HeroBComponent],
  imports: [CommonModule, Module1RoutingModule],
})
export class Module1Module {}
