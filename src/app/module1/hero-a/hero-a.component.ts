import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-hero-a',
  templateUrl: './hero-a.component.html',
  styleUrl: './hero-a.component.scss',
})
export class HeroAComponent {
  @Output() newMessageEvent = new EventEmitter<string>();

  Hello() {
    this.newMessageEvent.emit('Hello, I am Hero A');
    console.log('newMessageEvent: ', this.newMessageEvent);
  }
}
