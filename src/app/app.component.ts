import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  mFromHeroA!: string;
  wFromPerA!: string;

  receiveMessage(hello: string) {
    this.mFromHeroA = hello;
  }

  receiveWarn(warnNotify: string) {
    this.wFromPerA = warnNotify;
  }
}
